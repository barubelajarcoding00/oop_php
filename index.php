<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br>" . "<br>";

$sungokong = new Ape("Kera Sakti");

echo "Name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->kaki . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : " . $sungokong->yell . "<br>" . "<br>";

$kodok = new Frog("buduk");

echo "name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->suku . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : " . $kodok->jump . "<br>";