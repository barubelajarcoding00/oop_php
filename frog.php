<?php

require_once('animal.php');

class Frog extends Animal
{
    public $name;
    public $suku = 4;
    public $cold_blooded = "no";
    public $jump = "hop-hop";
}